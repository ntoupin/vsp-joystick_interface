var haveEvents = 'ongamepadconnected' in window;
var controllers = {};
var Last_Index = -1;

function Connect_Handler(e) {
    Add_Gamepad(e.gamepad);
}

function Add_Gamepad(gamepad) {

    // Add controller to list
    controllers[gamepad.index] = gamepad;

    // Save index for 
    Last_Index = gamepad.index;

    // Add controller infos to top banner
    var Gamepad_Info = document.getElementById('Gamepad_Info');
    Gamepad_Info.innerHTML = "Contrôleur connecté avec l'index " + gamepad.index + ": " + gamepad.id + ".";

    requestAnimationFrame(Update_Status);
}

function Disconnect_Handler(e) {
    Remove_Gamepad(e.gamepad);
}

function Remove_Gamepad(gamepad) {
    var d = document.getElementById("controller" + gamepad.index);
    document.body.removeChild(d);
    delete controllers[gamepad.index];
}

function Update_Status() {

    if (!haveEvents) {
        Scan_Gamepads();
    }

    var j;

    for (j in controllers) {

        var controller = controllers[j];

        txb_JoyX.value = Math.round(Map(controller.axes[0], -1, 1, 0, 4095));
        txb_JoyY.value = Math.round(Map(controller.axes[1], -1, 1, 0, 4095));
        txb_Twist.value = Math.round(Map(controller.axes[5], -1, 1, 0, 4095));
        txb_Throttle.value = Math.round(Map(controller.axes[6], -1, 1, 0, 4095));
        txb_Hat.value = Math.round(Map(controller.axes[9], -1, 1, 0, 4095));

        txb_Btn1.value = controller.buttons[0].value;
        txb_Btn2.value = controller.buttons[1].value;
        txb_Btn3.value = controller.buttons[2].value;
        txb_Btn4.value = controller.buttons[3].value;
        txb_Btn5.value = controller.buttons[4].value;
        txb_Btn6.value = controller.buttons[5].value;
        txb_Btn7.value = controller.buttons[6].value;
        txb_Btn8.value = controller.buttons[7].value;
        txb_Btn9.value = controller.buttons[8].value;
        txb_Btn10.value = controller.buttons[9].value;
        txb_Btn11.value = controller.buttons[10].value;
        txb_Btn12.value = controller.buttons[11].value;
    }

    requestAnimationFrame(Update_Status);
}

function Scan_Gamepads() {
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
    for (var i = 0; i < gamepads.length; i++) {
        if (gamepads[i]) {
            if (gamepads[i].index in controllers) {
                controllers[gamepads[i].index] = gamepads[i];
            } else {
                Add_Gamepad(gamepads[i]);
            }
        }
    }
}

window.addEventListener("gamepadconnected", Connect_Handler);
window.addEventListener("gamepaddisconnected", Disconnect_Handler);

if (!haveEvents) {
    setInterval(Scan_Gamepads, 500);
}

function Map(x, in_min, in_max, out_min, out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}